<?php

// Setup capsule
if (!function_exists('capsule')) {
    function capsule()
    {
        $capsule = new \Illuminate\Database\Capsule\Manager;
        $capsule->addConnection(array(
            'driver'    => 'mysql',
            'host'      => DB_HOST,
            'database'  => DB_ELOQUENT,
            'username'  => DB_USER,
            'password'  => DB_PASSWORD,
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => ''
        ));
        return $capsule;
    }
}

// Setup plugin
if (!function_exists('path')) {
	function path() {
		$reflection = new \ReflectionClass(\Composer\Autoload\ClassLoader::class);
		$path =  dirname($reflection->getFileName(), 3);
		return $path;
	}
}

// Setup blade
if (!function_exists('blade')) {
    function blade($page, $variables=array())
    {
        $views = path(). '/resources/views';
        $cache = path() . '/storage/framework/views';
        $blade = new \eftec\bladeone\BladeOne($views, $cache, \eftec\bladeone\BladeOne::MODE_DEBUG);
        return $blade->run($page, $variables);
    }
}
if (!function_exists('view')) {
    function view($page, $variables=array())
    {
        echo blade($page, $variables);
    }
}
if (!function_exists('add')) {
    function add($page, $variables=array())
    {
        add_filter( 'the_content', function($content) use($page, $variables) {
            return $content.blade($page, $variables);
        }, 100);
    }
}

// Setup route
if (!function_exists('add_route')) {
    function add_route($url, $callback)
    {
        return Routes::map($url, $callback);
    }
}

// Setup validate
if (!function_exists('validate')) {
    function validate($data, $rules)
    {
        $filesystem = new \Illuminate\Filesystem\Filesystem();
        $fileloader = new \Illuminate\Translation\FileLoader($filesystem, path() . '/resources/lang');
        $fileloader->addNamespace('lang',  path() .  '/resources/lang');
        $fileloader->load(WP_LANG, 'validation', 'lang');
        $translator = new \Illuminate\Translation\Translator($fileloader, WP_LANG);
        $factory = new \Illuminate\Validation\Factory($translator);
        $presenceverifier = new \Illuminate\Validation\DatabasePresenceVerifier(capsule()->getDatabaseManager());
        $factory->setPresenceVerifier($presenceverifier);
        $validator = $factory->make($data, $rules);
        if ($validator->fails()) {
            setcookie('errors', json_encode($validator->errors()->all()), time()+1);
			setcookie('input', json_encode($data), time()+1);
            wp_safe_redirect(wp_get_referer());
            die;
        }
        return true;
    }
}

// Setup old
if (!function_exists('old')) {
    function old($name, $default='')
    {
		$input = array();
		if (isset($_COOKIE['input'])) {
			$input = getcookie('input');
		}
        if (!empty($input[$name])) {
            $value = $input[$name];
        } else {
            $value =  $default;
        }
        return $value;
    }
}

// Setup getcookie
if (!function_exists('getcookie')) {
    function getcookie($name)
    {
		$input = array();
		if (isset($_COOKIE[$name]) && $result = json_decode(stripslashes($_COOKIE[$name]), true)) {
			return $result;
		}	elseif (isset($_COOKIE[$name]) && $result = stripslashes($_COOKIE[$name])) {
			return $result;
		} else {
			return null;
		}
    }
}

// Setup request
if (!function_exists('request')) {
    function request($key=null, $default=null)
    {
        $_POST = array_map(function ($item){
			if($item != '' and is_string($item)) {
				return stripslashes($item);
			} elseif(is_array($item)) {
				return $item;
			} else {
				return null;
			}
        }, $_POST);
        $request = \Illuminate\Http\Request::capture();
        if($key) {
            $result = $request->{$key};
            if(empty($result)) {
                $result = $default;
            }
        } else {
            $result = $request;
        }
        return $result;
    }
}

// Autoload String helpers (for some reason composers fails to do so)
require_once(path().'/vendor/illuminate/support/Str.php');

// Boot!
$capsule = capsule();
$capsule->setEventDispatcher(new \Illuminate\Events\Dispatcher(new \Illuminate\Container\Container));
$capsule->setAsGlobal();
$capsule->bootEloquent();

// Autoload Controllers!
foreach (glob(path()."/app/Http/Controllers/*.php") as $file) {
	$classes_old = get_declared_classes();
    include $file;
	$classes_new = get_declared_classes(); 
	$classes_diff = array_diff($classes_new, $classes_old);
	if (sizeof($classes_diff)) {
		foreach($classes_diff as $class) {
			if (substr($class, -strlen(basename($file, ".php"))) === basename($file, ".php")) {
				new $class;
			}
		}
	}
}
