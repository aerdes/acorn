<?php 

namespace aerdes\acorn\Http\Controllers;

class BaseController
{

    public function __construct() {
        add_action('init', function(){
			if (method_exists($this, 'actions_user')) {
				 $this->actions_user();
			}
            if (is_user_logged_in() and method_exists($this, 'actions_admin')) {
                $this->actions_admin();
            }
        });
    }
	
}
