# Acorn

Acorn is a Laravel-based framework for WordPress plugins.
Actually it is not a framework yet but just a collection of helper functions...

Inspiration originated from the promising [Acorn](https://roots.io/acorn/) project created by the [Roots team](https://roots.io/) which unfortunately is still in early development at the time of coding this framework (and which was missing some desperately needed features).

To get started make sure to define the following global variables (e.g. in the *wp_config.php*) which are needed for the booting the functionalities. The database is further assumed to have an utf8 charset and utf8_unicode_ci collation without prefixes.

- DB_ELOQUENT: The name of the database used
- DB_USER: MySQL database username
- DB_PASSWORD: MySQL database password
- DB_HOST: MySQL hostname
- WP_LANG: The locale used for e.g. validation features

Then install this composer package in your plugin directory at *wp-content/plugins/plugin-name*.
Also copy the *resouces* and *storage* directories from this composer package to your plugin directory.

Finally initialize the framework in your main plugin file at *wp-content/plugins/plugin-name/plugin-name.php* by autoloading everything (via `require_once(__DIR__ . '/vendor/autoload.php');`).
